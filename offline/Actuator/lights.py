
################################################################################
# This software uses BeautifulHue
# 
# 
# BeautifulHue License
# 
# Copyright (c) 2013 Allan Bunch
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE
# 
################################################################################

'''
    File name: lights.py
    Author(s): Bernardo Abreu
    Date created: Jan 2017
    Date last modified: Jan 17, 2017
    Python version: 2.7
'''

#Lights Api
from beautifulhue.api import Bridge

class Light():
    """ This class contains the lights information and a funtion to update them
    
    This class contains a list of dictionaries containing the state of each lamp
    connected. It provides functions to obtain and update information on status,
    color and brighness, and a funtion to update the lights with the current
    information
    """

    def __init__(self, light, light_ip = '192.168.0.87' ):
        
        self.bridge = Bridge(device={'ip': light_ip}, 
            user={'name': 'go3D6jUyb3yLQFP0tcPmJ3xzNPIC507T1SL2pnir'})

        self.light = light

        self.set_state()

        self.set_sat(254)


    def __str__(self):
        return 'Light ' + str(self.light) + \
            ': on = ' + str(self.get_status()) +\
            ', color (hue) = ' + str(self.get_color()) + \
            ', brightness = ' + str(self.get_bri())

    def get_state(self):
        return self.state


    def set_state(self):
        resource = {'which': self.light, 'verbose': True}

        self.state = self.bridge.light.get(resource)['resource']['state']


    def set_status(self, statusLamp):
        self.state['on'] = statusLamp

    def get_status(self):
        return self.state['on']

    def set_color(self, color):
        self.state['hue'] = color

    def get_color(self):
        return self.state['hue']

    def set_bri(self, bri):
        self.state['bri'] = bri

    def get_bri(self):
        return self.state['bri']

    def set_sat(self, sat):
        self.state['sat'] = sat

    def get_sat(self):
        return self.state['sat']

    def update_light(self):
        resource = {
            'which': self.light,
            'data': {
                'state':{
                            'on': self.get_status(),
                            'hue': self.get_color(),
                            'bri': self.get_bri(),
                            'sat' : self.get_sat()
                        }
            }
        }
        self.bridge.light.update(resource)