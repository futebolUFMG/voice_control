#!/usr/bin/env python

################################################################################
# This software uses BeautifulHue
# 
# 
# BeautifulHue License
# 
# Copyright (c) 2013 Allan Bunch
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE
# 
################################################################################

'''
	File name: actuator.py
	Author(s): Bernardo Abreu and Matheus Nunes
	Date created: Jan 2017
	Date last modified: Jan 17, 2017
	Python version: 2.7
'''

#Networking
import connection
# argparse to parse the argunments for the logging
import argparse
#Logging
import logging
import vc_logging
import os
#Lamp API
from beautifulhue.api import Bridge

class Actuator():
	"""  

	Actuator node class.

	"""
	def __init__(self, args, HOST = '', PORT = 57000, LIGHT_IP = '192.168.0.87'):
		vc_logging.init_logger(level = args.log_level, verbose = args.verbose)
		self.log = logging.getLogger("vc_logger")
		self.HOST = HOST
		self.PORT = PORT
		self.args = args
		self.light_ip = LIGHT_IP
		self.color = {
						'red'       : 0,
						'yellow'    : 14000,
						'green'     : 25500,
						'white'		: 35000,
						'blue'      : 47000,
						'pink'      : 56100
					}



	def update_lamp(self, lamp, d):
		#IP: Philips Hue Bridge (the small curcular device that connects to the lamps) IP
		bridge = Bridge(device={'ip': self.light_ip}, 
						user={'name': 'go3D6jUyb3yLQFP0tcPmJ3xzNPIC507T1SL2pnir'})
		resource = {
			'which': lamp,
			'data': {
				'state': d
			}
		}
		bridge.light.update(resource)
		pass


	def convert_command(self, command):
		
		command_dict = {}
		c = command.split('|')

		#Light
		if c[1] == 'all':
			lamp = list(range(1,4))
		else:
			lamp = c[1].split(',')
		
		#State
		if c[2]:
			command_dict['on'] = True if c[2] == 'on' else False
		
		#Color
		if c[3]:
			command_dict['hue'] = self.color[c[3]]
			command_dict['sat'] = 254
		
		#Brightness
		if c[4]:
			command_dict['bri'] = int((int(c[4])/100.0)*254)
		
		return (lamp,command_dict)


	def main(self):
		
		HOST = "192.168.0.98" #IP of Bristol VM
		PORT = 5008

		conn = connection.Client(HOST, PORT)

		try:
			print 'Opening connection'

			conn.connect()

			while True:

				command = conn.receive_message()

				if command != None: 

					self.log.info('Received: ' + str(command))

					data = command.split('/')

					for d in data:
						if d:
							lamp,command_dict = self.convert_command(d)
							if command_dict:		#False if command_dict is empty
								for l in lamp:
									self.log.info('lamp: ' + str(lamp)
													+ '\n' + str(command_dict))
									self.update_lamp(l, command_dict)
			
		except KeyboardInterrupt:
			print '\nINTERRUPTED BY USER'		
		except Exception as e:
			self.log.debug("ERROR: " + str(e))
		finally:
			conn.destroy()
			self.log.debug('Finishing program')



if(__name__ == '__main__'):
	try:
		parser = argparse.ArgumentParser(description='Actuator Node Logging')
		parser.add_argument('--log-level', action="store", type=str, choices=["critical", "error", "warning", "info", "debug", "notset"], default="info", help='Select the log level of the program.')
		parser.add_argument('--verbose', default=False, action = 'store_true', help='Select whether to output logs to the console.')

		args = parser.parse_args()

	except Exception as e:
		print 'ERROR PARSING ARGUMENTS'
	act = Actuator(args)
	act.main()